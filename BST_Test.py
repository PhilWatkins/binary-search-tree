import unittest
from Binary_Search_Tree import Binary_Search_Tree

class BST_Tester(unittest.TestCase):
    def setUp(self):
        self.__BST = Binary_Search_Tree()

    def test_height_initially_zero(self):
        self.assertEqual(self.__BST.get_height(), 0)

    def test_insert_on_empty(self):
        self.__BST.insert_element(1)
        self.assertEqual(self.__BST.in_order(), "[ 1 ]")

    def test_insert_2_elements(self):
        self.__BST.insert_element("One fish")
        self.__BST.insert_element("Two fish")
        self.assertEqual(self.__BST.in_order(), "[ One fish, Two fish ]")

    def test_height_is_one_after_inserting_first_val(self):
        self.__BST.insert_element("Value")
        self.assertEqual(self.__BST.get_height(), 1)

    def test_height_is_two(self):
        self.__BST.insert_element(1)
        self.__BST.insert_element(2)
        self.assertEqual(self.__BST.get_height(), 2)

    def test_height_is_two_one_child_each_side(self):
        self.__BST.insert_element(90)
        self.__BST.insert_element(91)
        self.__BST.insert_element(0)
        self.assertEqual(self.__BST.get_height(), 2)
        
    def test_raises_val_error_ins_repeated_val(self):
        self.__BST.insert_element(1)
        with self.assertRaises(ValueError):
            self.__BST.insert_element(1)

    def test_inord_trav_empty(self):
        self.assertEqual(self.__BST.in_order(), "[ ]")

    def test_postord_trav_empty(self):
        self.assertEqual(self.__BST.post_order(), "[ ]")

    def test_preord_trav_empty(self):
        self.assertEqual(self.__BST.pre_order(), "[ ]")

    def test_inord_trav_single_element(self):
        self.__BST.insert_element(12)
        self.assertEqual(self.__BST.in_order(), "[ 12 ]")

    def test_preord_trav_single_element(self):
        self.__BST.insert_element(9000)
        self.assertEqual(self.__BST.pre_order(), "[ 9000 ]")

    def test_post_order_trav_single_element(self):
        self.__BST.insert_element(47)
        self.assertEqual(self.__BST.post_order(), "[ 47 ]")

    def test_inord_trav_2_elements(self):
        self.__BST.insert_element(19)
        self.__BST.insert_element(7)
        self.assertEqual(self.__BST.in_order(), "[ 7, 19 ]")

    def test_preord_trav_2_elements(self):
        self.__BST.insert_element(19)
        self.__BST.insert_element(7)
        self.assertEqual(self.__BST.pre_order(), "[ 19, 7 ]")

    def test_post_order_trav_2_elements(self):
        self.__BST.insert_element(19)
        self.__BST.insert_element(7)
        self.assertEqual(self.__BST.post_order(), "[ 7, 19 ]")

    def test_remove_main_root_of_1_node_tree(self):
        self.__BST.insert_element(200)
        self.__BST.remove_element(200)
        self.assertEqual(self.__BST.in_order(), "[ ]")

    def test_raises_error_rem_value_not_in_tree(self):
        self.__BST.insert_element(19)
        self.__BST.insert_element(7)
        self.__BST.insert_element(43)
        self.__BST.insert_element(22)
        self.__BST.insert_element(47)
        self.__BST.insert_element(77)
        self.__BST.remove_element(47)
        with self.assertRaises(ValueError):
            self.__BST.remove_element(6)

    def test_raises_error_rem_value_empty(self):
        with self.assertRaises(ValueError):
            self.__BST.remove_element(90)

    def test_tree_insert_strings(self):
        self.__BST.insert_element("a")
        self.__BST.insert_element("Z")
        self.assertEqual(self.__BST.in_order(), "[ Z, a ]")

    def test_height_after_emptying_then_repopulating(self):
        self.__BST.insert_element(12)
        self.__BST.insert_element(10)
        self.__BST.insert_element(13)
        self.__BST.insert_element(11)
        self.__BST.remove_element(12)
        self.__BST.remove_element(10)
        self.__BST.remove_element(13)
        self.__BST.remove_element(11)
        self.__BST.insert_element(765)
        self.__BST.insert_element(-2)
        self.assertEqual(self.__BST.get_height(), 2)

    def test_height_after_inorder_trav(self):
        self.__BST.insert_element(1)
        self.__BST.insert_element(2)
        self.__BST.insert_element(-1)
        self.__BST.insert_element(1.5)
        self.assertEqual(self.__BST.get_height(), 3)
        self.__BST.in_order()
        self.assertEqual(self.__BST.get_height(), 3)

    def test_height_after_preord_trav(self):
        self.__BST.insert_element(1)
        self.__BST.insert_element(2)
        self.__BST.insert_element(-1)
        self.__BST.insert_element(1.5)
        self.assertEqual(self.__BST.get_height(), 3)
        self.__BST.pre_order()
        self.assertEqual(self.__BST.get_height(), 3)

    def test_height_after_postord_trav(self):
        self.__BST.insert_element(1)
        self.__BST.insert_element(2)
        self.__BST.insert_element(-1)
        self.__BST.insert_element(1.5)
        self.assertEqual(self.__BST.get_height(), 3)
        self.__BST.post_order()
        self.assertEqual(self.__BST.get_height(), 3)

    def test_height_before_and_after_removing_main_root(self):
        self.__BST.insert_element(1)
        self.__BST.insert_element(0)
        self.__BST.insert_element(2)
        self.__BST.insert_element(70)
        self.__BST.insert_element(-70)
        self.assertEqual(self.__BST.get_height(), 3)
        self.__BST.remove_element(1)
        self.assertEqual(self.__BST.get_height(), 3)
        self.__BST.remove_element(-70)
        self.assertEqual(self.__BST.get_height(), 2)

    def test_inorder_trav_after_deletions(self):
        self.__BST.insert_element("a")
        self.__BST.insert_element("c")
        self.__BST.insert_element("d")
        self.__BST.insert_element("C")
        self.__BST.insert_element("g")
        self.__BST.insert_element("F")
        self.__BST.insert_element("p")
        self.__BST.insert_element("r")
        self.__BST.remove_element("C")
        self.__BST.remove_element("a")
        self.assertEqual(self.__BST.in_order(), "[ F, c, d, g, p, r ]")

    def test_tree_height_removing_all_nodes(self):
        self.__BST.insert_element(19)
        self.__BST.insert_element(7)
        self.__BST.insert_element(43)
        self.__BST.insert_element(90)
        self.__BST.insert_element(100000000000)
        self.__BST.insert_element(75)
        self.__BST.insert_element(-345)

        self.__BST.remove_element(19)
        self.__BST.remove_element(7)
        self.__BST.remove_element(43)
        self.__BST.remove_element(90)
        self.__BST.remove_element(100000000000)
        self.__BST.remove_element(75)
        self.__BST.remove_element(-345)
        self.assertEqual(self.__BST.get_height(), 0)

    # project 5 specific tests (but some of the above apply to balanced & imbalanced (like empty tree h=0)
    def test_list_representation_4_elems(self):
        self.__BST.insert_element(12)
        self.__BST.insert_element(-90)
        self.__BST.insert_element(100)
        self.__BST.insert_element(101)
        self.assertEqual(self.__BST.to_list(), [-90, 12, 100, 101])

    def test_list_empty(self):
        self.assertEqual(self.__BST.to_list(), [])

    def test_list_1_element(self):
        self.__BST.insert_element(142)
        self.assertEqual(self.__BST.to_list(), [142])

    def test_list_2_elements(self):
        self.__BST.insert_element(8)
        self.__BST.insert_element(32423)
        self.assertEqual(self.__BST.to_list(), [8, 32423])

    def test_list_3_elements(self):
        self.__BST.insert_element("a")
        self.__BST.insert_element("Z")
        self.__BST.insert_element("r")
        self.assertEqual(self.__BST.to_list(), ["Z", "a", "r"])

    def test_single_left_rotation(self):
        self.__BST.insert_element(12)
        self.__BST.insert_element(11)
        self.__BST.insert_element(14)
        self.__BST.insert_element(13)
        self.__BST.insert_element(23)
        self.__BST.insert_element(15)
        self.assertEqual(self.__BST.in_order(), "[ 11, 12, 13, 14, 15, 23 ]")
        self.assertEqual(self.__BST.pre_order(), "[ 14, 12, 11, 13, 23, 15 ]")
        self.assertEqual(self.__BST.post_order(), "[ 11, 13, 12, 15, 23, 14 ]")

    def test_simple_left_rot(self):
        self.__BST.insert_element(1)
        self.__BST.insert_element(2)
        self.__BST.insert_element(3)
        self.assertEqual(self.__BST.pre_order(), "[ 2, 1, 3 ]")

    def test_get_height_balanced(self):
        self.__BST.insert_element(1)
        self.__BST.insert_element(2)
        self.__BST.insert_element(3)
        self.assertEqual(self.__BST.get_height(), 2)

    def test_height_after_single_left_rotation(self):
        self.__BST.insert_element(12)
        self.__BST.insert_element(11)
        self.__BST.insert_element(14)
        self.__BST.insert_element(13)
        self.__BST.insert_element(23)
        self.__BST.insert_element(15)
        self.assertEqual(self.__BST.get_height(), 3)

    def test_height_inserting_9_increasing(self):
        for i in range(1, 10):
            self.__BST.insert_element(i)
        self.assertEqual(self.__BST.get_height(), 4)
        self.assertEqual(self.__BST.pre_order(), "[ 4, 2, 1, 3, 6, 5, 8, 7, 9 ]")
        self.assertEqual(self.__BST.in_order(), "[ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]")

    def test_list_9_values_right_rotations_req(self):
        self.__BST.insert_element(-1)
        self.assertEqual(self.__BST.to_list(), [-1])
        self.__BST.insert_element(-2)
        self.assertEqual(self.__BST.to_list(), [-2,-1])
        self.__BST.insert_element(-3)
        self.assertEqual(self.__BST.to_list(), [-3,-2,-1])
        self.__BST.insert_element(-4)
        self.assertEqual(self.__BST.to_list(), [-4,-3,-2,-1])
        self.__BST.insert_element(-5)
        self.assertEqual(self.__BST.to_list(), [-5,-4,-3,-2,-1])
        self.__BST.insert_element(-6)
        self.__BST.insert_element(-7)
        self.__BST.insert_element(-8)
        self.__BST.insert_element(-9)
        self.assertEqual(self.__BST.to_list(), [-9,-8,-7,-6,-5,-4,-3,-2,-1])
        self.assertEqual(self.__BST.pre_order(), "[ -4, -6, -8, -9, -7, -5, -2, -3, -1 ]")
        self.assertEqual(self.__BST.post_order(), "[ -9, -7, -8, -5, -6, -3, -1, -2, -4 ]")

    def test_left_right_rotation(self):
        self.__BST.insert_element(7)
        self.__BST.insert_element(8)
        self.__BST.insert_element(1)
        self.__BST.insert_element(4)
        self.__BST.insert_element(0)
        self.__BST.insert_element(5) # triggers double rotation
        self.assertEqual(self.__BST.pre_order(), "[ 4, 1, 0, 7, 5, 8 ]")
        self.assertEqual(self.__BST.in_order(), "[ 0, 1, 4, 5, 7, 8 ]")

    def test_right_left_rotation(self):
        self.__BST.insert_element(12)
        self.__BST.insert_element(7)
        self.__BST.insert_element(20)
        self.__BST.insert_element(15)
        self.__BST.insert_element(40)
        self.__BST.insert_element(80) # triggers rotation
        self.assertEqual(self.__BST.pre_order(), "[ 20, 12, 7, 15, 40, 80 ]")
        self.assertEqual(self.__BST.post_order(), "[ 7, 15, 12, 80, 40, 20 ]")
        self.assertEqual(self.__BST.to_list(), [7, 12, 15, 20, 40, 80])

    def test_height_after_LR_rotat(self):
        self.__BST.insert_element(7)
        self.assertEqual(self.__BST.get_height(), 1)
        self.__BST.insert_element(8)
        self.assertEqual(self.__BST.get_height(), 2)
        self.__BST.insert_element(1)
        self.assertEqual(self.__BST.get_height(), 2)
        self.__BST.insert_element(4)
        self.assertEqual(self.__BST.get_height(), 3)
        self.__BST.insert_element(0)
        self.assertEqual(self.__BST.get_height(), 3)
        self.__BST.insert_element(5) # triggers double rotation
        self.assertEqual(self.__BST.get_height(), 3)   

    def test_height_after_RL_rotat(self):
        self.__BST.insert_element(12)
        self.assertEqual(self.__BST.get_height(), 1)
        self.__BST.insert_element(7)
        self.assertEqual(self.__BST.get_height(), 2)
        self.__BST.insert_element(20)
        self.assertEqual(self.__BST.get_height(), 2)
        self.__BST.insert_element(15)
        self.assertEqual(self.__BST.get_height(), 3)
        self.__BST.insert_element(40)
        self.assertEqual(self.__BST.get_height(), 3)
        self.__BST.insert_element(80) # triggers rotation        
        self.assertEqual(self.__BST.get_height(), 3)

    def test_removal_L_rotation(self):
        self.__BST.insert_element(4)
        self.__BST.insert_element(5)
        self.__BST.insert_element(3)
        self.__BST.insert_element(7)
        self.__BST.remove_element(3)
        self.assertEqual(self.__BST.in_order(), "[ 4, 5, 7 ]")
        self.assertEqual(self.__BST.pre_order(), "[ 5, 4, 7 ]")

    def test_removal_R_rotation(self):
        self.__BST.insert_element(40)
        self.__BST.insert_element(35)
        self.__BST.insert_element(1000)
        self.__BST.insert_element(30)
        self.__BST.remove_element(1000)
        self.assertEqual(self.__BST.to_list(), [30,35,40])
        self.assertEqual(self.__BST.post_order(), "[ 30, 40, 35 ]")
        self.assertEqual(self.__BST.pre_order(), "[ 35, 30, 40 ]")

    def test_height_b4_and_aft_removal(self):
        self.__BST.insert_element(40)
        self.__BST.insert_element(35)
        self.__BST.insert_element(1000)
        self.__BST.insert_element(30)
        self.assertEqual(self.__BST.get_height(), 3)
        self.__BST.remove_element(1000)
        self.assertEqual(self.__BST.get_height(), 2)

    def test_structure_large_tree(self):
        self.__BST.insert_element(25)
        self.__BST.insert_element(30)
        self.__BST.insert_element(31)
        self.__BST.insert_element(50)
        self.__BST.insert_element(45)
        self.assertEqual(self.__BST.pre_order(), "[ 30, 25, 45, 31, 50 ]")
        self.__BST.insert_element(55)
        self.__BST.insert_element(34)
        self.__BST.remove_element(50)
        self.__BST.remove_element(25)
        self.__BST.remove_element(30)
        self.assertEqual(self.__BST.pre_order(), "[ 45, 31, 34, 55 ]")
        self.assertEqual(self.__BST.in_order(), "[ 31, 34, 45, 55 ]")
        self.__BST.insert_element(30)
        self.assertEqual(self.__BST.post_order(), "[ 30, 34, 31, 55, 45 ]")
        self.assertEqual(self.__BST.pre_order(), "[ 45, 31, 30, 34, 55 ]")

    def test_if_checkpoint_tree_replicated(self): # test if I can replicate answers from the checkpoint
        self.__BST.insert_element(38)
        self.__BST.insert_element(17)
        self.__BST.insert_element(93)
        self.__BST.insert_element(8)
        self.__BST.insert_element(27)
        self.__BST.insert_element(73)
        self.__BST.insert_element(139)
        self.__BST.insert_element(3)
        self.__BST.insert_element(12)
        self.__BST.insert_element(21)
        self.__BST.insert_element(85)
        self.__BST.insert_element(112)
        self.__BST.insert_element(140)
        self.__BST.insert_element(15)
        self.__BST.insert_element(124)
        
        self.__BST.remove_element(38)
        self.__BST.remove_element(8)
        self.__BST.remove_element(17)
        self.__BST.remove_element(21)
        self.assertEqual(self.__BST.pre_order(), "[ 73, 12, 3, 27, 15, 112, 93, 85, 139, 124, 140 ]")
        self.assertEqual(self.__BST.to_list(), [3,12,15,27,73,85,93,112,124,139,140])
        self.assertEqual(self.__BST.in_order(), "[ 3, 12, 15, 27, 73, 85, 93, 112, 124, 139, 140 ]")
        self.assertEqual(self.__BST.post_order(), "[ 3, 15, 27, 12, 85, 93, 124, 140, 139, 112, 73 ]")
        
    def test_height_checkpoint_tree(self):
        self.__BST.insert_element(38)
        self.__BST.insert_element(17)
        self.__BST.insert_element(93)
        self.__BST.insert_element(8)
        self.__BST.insert_element(27)
        self.__BST.insert_element(73)
        self.__BST.insert_element(139)
        self.__BST.insert_element(3)
        self.__BST.insert_element(12)
        self.__BST.insert_element(21)
        self.__BST.insert_element(85)
        self.__BST.insert_element(112)
        self.__BST.insert_element(140)
        self.__BST.insert_element(15)
        self.__BST.insert_element(124)
        
        self.__BST.remove_element(38)
        self.__BST.remove_element(8)
        self.__BST.remove_element(17)
        self.__BST.remove_element(21)
        self.assertEqual(self.__BST.get_height(), 4)

    def test_303_hw(self):
        self.__BST.insert_element(12)
        self.__BST.insert_element(10)
        self.__BST.insert_element(15)
        self.__BST.insert_element(17)
        self.__BST.insert_element(19)
        self.__BST.insert_element(14)
        self.__BST.insert_element(18)
        self.assertEqual(self.__BST.get_height(), 3)
        
if __name__ == '__main__':
    unittest.main()
