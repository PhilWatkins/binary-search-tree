class Binary_Search_Tree:

  ## Below is code for the Node class ##
  class __BST_Node:
    def __init__(self, value): 
      """Node constructor"""
      self.value = value
      self.left = None
      self.right = None
      self.height = 1

    def update_node_height(self):
      """__Node class method: Used to update heights of nodes along the affected path."""
      self.height = max(self.__get_node_height(self.left), self.__get_node_height(self.right)) + 1

    def __get_node_height(self, node):
      if node is None:
        return 0 # Treat height is 0 in calculations to prevent errors 
      return node.height

    def compute_balance(self, node):
      return self.__get_node_height(node.right) - self.__get_node_height(node.left)


  ## Below is code that belongs to the binary search tree not the Node class ##

  def __init__(self):
    """Tree constructor"""
    self.__root = None

  def insert_element(self, value):
    if self.__root is None: 
      self.__root = Binary_Search_Tree.__BST_Node(value)
    else:
      self.__insertion_helper(value, self.__root)
      self.__root.update_node_height()
      self.__root = self.__balance(self.__root)
    
  def __insertion_helper(self, value, t):
    if value < t.value:
      if t.left is None:
        t.left = Binary_Search_Tree.__BST_Node(value)
        # return t.left
      else:
        self.__insertion_helper(value, t.left)
        t.left.update_node_height() # update subtree height BEFORE returning
        t.left = self.__balance(t.left)
      return t.left
      
    if value > t.value:
      if t.right is None:
        t.right = Binary_Search_Tree.__BST_Node(value)
        # return t.right
      else:
        self.__insertion_helper(value, t.right)
        t.right.update_node_height() # update subtree height BEFORE returning
        t.right = self.__balance(t.right)
      return t.right

    else: # must be a repeated value
      raise ValueError

  def remove_element(self, value):
    if self.__root is None:
      raise ValueError
    self.__root = self.__removal_helper(value, self.__root)
    if self.__root is not None:
      self.__root.update_node_height()
      self.__balance(self.__root)

  def __removal_helper(self, value, root):
    """Recursive function to help public removal. Updates height before returning each subtree along removal path"""
    if root is None: # value was not found
      raise ValueError

    if value > root.value:
      root.right = self.__removal_helper(value, root.right)
      root.update_node_height()
      return self.__balance(root)

    elif value < root.value:
      root.left = self.__removal_helper(value, root.left)
      root.update_node_height()
      return self.__balance(root)

    elif root.value == value:
      if root.left is not None and root.right is not None: # 2 children
        current = root.right
        while current.left is not None: # find min from right
          current = current.left
        root.value = current.value # put value in correct place
        root.right = self.__removal_helper(current.value, root.right) # remove duplicate from right subtree
        root.update_node_height()
        return self.__balance(root)

      elif root.left is None and root.right is not None: # right child only
        root.right.update_node_height()
        return self.__balance(root.right)
      elif root.left is not None and root.right is None: # left child only
        root.left.update_node_height()
        return self.__balance(root.left)
      else: # no children
        return None

  def in_order(self):
    """Left, Parent, Right"""
    if self.__root is None:
      return "[ ]"
    to_return = self.__in_order_helper(self.__root)    
    return "[ " + to_return[:-2] + " ]" # [:-2] part removes the comma from last value so [ 1 ,2,  ]->[ 1, 2 ]

  def __in_order_helper(self, root):
    string_representation = ""
    if root is not None:
      string_representation += self.__in_order_helper(root.left)
      string_representation += str(root.value) + ", "
      string_representation += self.__in_order_helper(root.right)
    return string_representation

  def pre_order(self):
    """Parent, Left, Right"""
    if self.__root is None:
      return "[ ]"
    to_return = self.__pre_order_helper(self.__root)
    return "[ " + to_return[:-2] + " ]"

  def __pre_order_helper(self, root):
    string_representation = ""
    if root is not None:
      string_representation += str(root.value) + ", "
      string_representation += self.__pre_order_helper(root.left)
      string_representation += self.__pre_order_helper(root.right)
    return string_representation

  def post_order(self):
    """Left, Right, Parent"""
    if self.__root is None:
      return '[ ]'
    to_return = self.__post_order_helper(self.__root)
    return "[ " + to_return[:-2] + " ]"

  def __post_order_helper(self, root):
    string_representation = ""
    if root is not None:
      string_representation += self.__post_order_helper(root.left)
      string_representation += self.__post_order_helper(root.right)
      string_representation += str(root.value) + ", "
    return string_representation

  def get_height(self):
    """Returns height of root that will get updated to the maximum height of its children +1 after ins/removal"""
    if self.__root is not None:
      return self.__root.height
    return 0

  def __str__(self):
    return self.in_order()

  def __right_rotate(self, root):
    new_root = root.left
    root.left = new_root.right
    new_root.right = root
    root.update_node_height()
    new_root.update_node_height()
    return new_root

  def __left_rotate(self, root):
    new_root = root.right
    root.right = new_root.left
    new_root.left = root
    root.update_node_height()
    new_root.update_node_height()
    return new_root

  def __balance(self, root):
    if root.compute_balance(root) >= 2: # POSITIVE BALANCE
      if root.right.compute_balance(root.right) >= 0: # single left rotation
        return self.__left_rotate(root)
      elif root.right.compute_balance(root.right) < 0: # right left rotation
        root.right = self.__right_rotate(root.right)
        return self.__left_rotate(root)

    elif root.compute_balance(root) <= -2: # NEGATIVE BALANCE
      if root.left.compute_balance(root.left) <= 0: # single right rotation
        return self.__right_rotate(root)
      elif root.left.compute_balance(root.left) > 0: # left right rotation
        root.left = self.__left_rotate(root.left)
        return self.__right_rotate(root)
    else:
      return root # no balancing required

  def to_list(self):
    if self.__root is None:
      return []
    else:
      self.__arr = [] # will become the final list

      # create list, pass list to bracket remover, then return the compressed list
      return self.__remove_extra_brackets(self.__to_list_helper(self.__root))

  def __to_list_helper(self, root):
    list_representation = []
    if root is not None:
      list_representation.append(self.__to_list_helper(root.left))
      list_representation.append(root.value)
      list_representation.append(self.__to_list_helper(root.right))
    return list_representation # will contain extra brackets ex. [[[], -90, []], 12, [[], 100, [[], 101, []]]]
                               # they will get removed recursively by the function below.

  def __remove_extra_brackets(self, list_representation):
    for i in list_representation:
      if type(i) != list:
        self.__arr.append(i)
      else:
        self.__remove_extra_brackets(i)
    return self.__arr

if __name__ == '__main__':
  pass
  # put this in a traversal to check the heights of each node along the return path, not just self.__root's height
  # print("value: ", root.value,"height: ", root.height)

